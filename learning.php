<?php

/**
   * Plugin Name:  Learning 
   * Description:  Learn the basics with this plugin.
   * Author:  Moin Memon
   * Author URI:  https://github.com/moinr
*/

class Learn{
    public function __construct() {
        $plugin = plugin_basename(__FILE__);
        
        // Hook into the admin menu
        add_action('admin_menu', array($this, 'learn_page'));

        // Add settings link
        add_filter('plugin_action_links_' . $plugin, array($this, 'add_settings_link'));

        // create sections 
        add_action('admin_init', array($this, 'setup_sections'));

        // add fields 
        add_action('admin_init', array($this, 'setup_fields'));
    }

    public function add_settings_link($links) {
        $settings_link = sprintf('<a href="admin.php?page=autofill">Settings</a>');
        array_unshift($links, $settings_link);
        return $links;
    }

    public function learn_page() {
        $page_title = 'Auto fill City & State';
        $menu_title = 'Auto fill';
        $capability = 'manage_options';
        $slug = 'autofill';
        $callback = array($this, 'learn_page_content');
        $icon = 'dashicons-admin-plugins';
        $position = 50;
        
        // Add the menu item and page into admin panel 
        add_menu_page($page_title, $menu_title, $capability, $slug, $callback, $icon, $position);
    }
    
    public function learn_page_content(){
    ?>
        <div class="wrap">
            <h2>Plugin setting page </h2>

            <?php
                $active_tab = 'tab1'; 
                if( isset( $_GET[ 'tab' ] ) ) {
                    $active_tab = $_GET['tab']; 
                } 
            ?>
            <!--  Plugin tabs -->
            <h2 class="nav-tab-wrapper">
                <a href="?page=autofill&tab=tab1" class="nav-tab <?php echo $active_tab == 'tab1' ? 'nav-tab-active' : ''; ?>">Tab - 1</a>
                <a href="?page=autofill&tab=tab2" class="nav-tab <?php echo $active_tab == 'tab2' ? 'nav-tab-active' : ''; ?>">Tab - 2 </a>
            </h2>

            <?php
                if (isset($_GET['settings-updated']) && $_GET['settings-updated']) {
                    $this->submission_feedback();
                }
            ?>
            <form method="POST" action="options.php">
                <?php
                
                // for conditional page display, to be optimized soon 
                if($active_tab == 'tab1'){
                    settings_fields('autofill');
                    do_settings_sections('autofill');
                }
                else{
                    settings_fields('autofill');
                    do_settings_sections('autofill2');
                }

                submit_button();

                ?>
            </form>
        </div>
    <?php
    }

    public function submission_feedback(){
    ?>
        <div class="notice notice-success is-dismissible">
            <p>Your settings have been updated!</p>
        </div>
    <?php
    }

    function setup_sections() {
        add_settings_section('first_section', 'Section - 1', array($this, 'section_callback'), 'autofill');
        add_settings_section('second_section', 'Section - 2', array($this, 'section_callback'), 'autofill2');
    }

    function section_callback($arguments){
        // Nothing right now.. 
    }

    public function setup_fields(){
        $fields = array(
            array(
                'uid' => 'first_field',
                'label' => 'First field',
                'section' => 'first_section',
                'type' => 'text',
                'options' => false,
                'optgroup' => 'autofill',
                'placeholder' => 'Some value here',
                'default' => ''
            ),
            array(
                'uid' => 'first_test_field',
                'label' => 'First field',
                'section' => 'second_section',
                'type' => 'text',
                'optgroup'=> 'autofill2',
                'options' => false,
                'placeholder' => 'Some value here',
                'default' => ''
            ),
            array(
                'uid' => 'second_field',
                'label' => 'Second field',
                'section' => 'first_section',
                'type' => 'number',
                'options' => false,
                'optgroup' => 'autofill',
                'placeholder' => 'Enter number',
                'default' => ''
            ),
            array(
                'uid' => 'third_field',
                'label' => 'third field',
                'section' => 'first_section',
                'type' => 'date',
                'options' => false,
                'optgroup' => 'autofill',
                'default' => ''
            ),
            array(
                'uid' => 'fourth_field',
                'label' => 'Fourth field',
                'section' => 'first_section',
                'type' => 'textarea',
                'options' => false,
                'optgroup' => 'autofill',
                'placeholder' => 'Enter text here',
                'default' => ''
            ),
            array(
                'uid' => 'fifth_field',
                'label' => 'Fifth field',
                'section' => 'first_section',
                'type' => 'select',
                'options' => array(
                    'a' => 'Option - 1',
                    'b' => 'Option - 2',
                    'c' => 'Option - 3'
                ),
                'placeholder' => 'Text goes here',
                'default' => 'a', 
                'optgroup' => 'autofill'
            ),
            array(
        		'uid' => 'sixth_field',
        		'label' => 'Sixth field',
        		'section' => 'first_section',
        		'type' => 'checkbox',
        		'options' => array(
        			'option1' => 'Option 1',
        			'option2' => 'Option 2',
        			'option3' => 'Option 3',
        			'option4' => 'Option 4',
        			'option5' => 'Option 5',
        		),
                'default' => array(), 
                'optgroup' => 'autofill'
        	), 
            array(
        		'uid' => 'seventh_field',
        		'label' => 'Seventh field',
        		'section' => 'first_section',
        		'type' => 'radio',
        		'options' => array(
        			'option1' => 'Option 1',
        			'option2' => 'Option 2',
        			'option3' => 'Option 3',
        			'option4' => 'Option 4',
        			'option5' => 'Option 5',
        		),
                'default' => array(),
                'optgroup' => 'autofill'
        	),
            array(
        		'uid' => 'eight_field',
        		'label' => 'Password field',
        		'section' => 'first_section',
        		'type' => 'password',
                'default' => '',
                'optgroup' => 'autofill'
        	)
        );
        
        foreach ($fields as $field) {
            add_settings_field($field['uid'], $field['label'], array($this, 'field_callback'), $field['optgroup'], $field['section'], $field);
            register_setting('autofill', $field['uid']);
        }
    }

    public function field_callback($arguments){

        $value = get_option($arguments['uid']); // Get the current value, if there is one
        
        if (!$value) { // If no value exists
            $value = $arguments['default']; // Set to our default
        }

        // Check which type of field we want
        switch ($arguments['type']) {
            case 'text':
            case 'password':
                printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
                break;

            case 'number':
                printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
                break;

            case 'date':
                printf('<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value);
                break;

            case 'textarea':
                printf('<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value);
                break;

            case 'checkbox':
            case 'radio':
                if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ){
                    $checkbox_markup = '';
                    $iterator = 0;
                    foreach( $arguments['options'] as $key => $label ){
                        $iterator++;
                        $checkbox_markup .= sprintf( '<label for="%1$s_%6$s"> <input id="%1$s_%6$s" name="%1$s[]" type="%2$s" value="%3$s" %4$s /> %5$s</label><br/>', $arguments['uid'], $arguments['type'], $key, checked( $value[ array_search( $key, $value, true ) ], $key, false ), $label, $iterator );
                    }
                    printf( '<fieldset>%s</fieldset>', $checkbox_markup );
                }
                break;

            case 'select':
                if (!empty($arguments['options']) && is_array($arguments['options'])) {
                    $options_markup = '';
                    foreach ($arguments['options'] as $key => $label) {
                        $options_markup .= sprintf('<option value="%s" %s>%s</option>', $key, selected($value, $key, false), $label);
                    }
                    printf('<select name="%1$s" id="%1$s">%2$s</select>', $arguments['uid'], $options_markup);
                }
                break;

        }


        // If there is help text
        if ($helper = $arguments['helper']) {
            printf('<span class="helper"> %s</span>', $helper); 
        }

        // If there is supplemental text
        if ($supplimental = $arguments['supplemental']) {
            printf('<p class="description">%s</p>', $supplimental); 
        }
    }
}

// Instantiated an object of plugin class 
new Learn();
